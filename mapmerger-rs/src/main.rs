extern crate image;

//use std::io::File;
use image::GenericImage;

fn main() {
    let img = image::open(&Path::new("/home/qbm368/Salem/map/game.salemthegame.com/2014-08-25 21.59.18/tile_0_0.png")).unwrap();
    println!("{} @ {}", img.dimensions(), img.color());
    let raw = img.raw_pixels();
    for y in range(0,100) {
        for x in range(0,100) {
            let r:u16 = raw[(y*100+x)*4] as u16;
            let g:u16 = raw[(y*100+x)*4+1] as u16;
            let b:u16 = raw[(y*100+x)*4+2] as u16;
            let tot = r + g + b;
            if tot == 0 { print!(" "); }
            else if tot < 255 { print!("-"); }
            else if tot < 512 { print!("*"); }
            else { print!("@"); }
        }
        println!("");
    }
}
