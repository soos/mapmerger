from os import listdir,rmdir,getcwd,rename,remove,makedirs
from os.path import isdir,isfile,splitext,basename,join,exists,getctime
from hashlib import md5
from shutil import copyfile, copystat, rmtree


def files(path):
  return [f for f in listdir(path) if isfile(join(path, f))]

def is_tile(file_name):
  (name, ext) = splitext(file_name)
  if name.startswith('tile_') and (name.count('_') == 2) and (ext == '.png'):
    return True
  return False

def name2coord(name):
  tmp = name.split('_')
  return (int(tmp[1]), int(tmp[2].split('.')[0]))

def dir_is_empty(path):
  if len(listdir(path)) == 0:
    return True
  return False


class struct:
  def __init__(self, **kwds):
    self.__dict__.update(kwds)


class mapmerger:
  def __init__(self, path):
    self.path = path
    sessions = [struct(name = d) for d in listdir(path) if isdir(join(path, d)) and not dir_is_empty(join(path, d))]
    for session in sessions:
      session.path = join(path, session.name)
      tiles = [struct(name = t) for t in filter(is_tile, files(session.path))]
      for tile in tiles:
        tile.path = join(session.path, tile.name)
        h = md5()
        h.update(open(tile.path, mode='rb').read())
        tile.md5 = h.digest()
        tile.x, tile.y = name2coord(tile.name)
        tile.ctime = getctime(tile.path)
        tile.dup = False
      #session.tiles = {(t.x, t.y): struct(name=t.name, path=t.path, md5=t.md5, ctime=t.ctime, dup=t.dup) for t in tiles }
      session.tiles = tiles
    self.sessions = [s for s in sessions if len(s.tiles) > 0]

  def mark_clean_tiles(self):
    for session in self.sessions:
      for tile in session.tiles:
        if (tile.md5 == b'\xd6\xb2\x8f&\x9c\xbfh\xabT\xdf\xd3\x8c\xf6[\x94\xc5') or \
           (tile.md5 == b'\xec\xaa\x8d_FH\xfd\xcd\x8bzV\xc5m\xaa\xa4,') or \
           (tile.md5 == b'\xf3\x94e\xcd\x14oOe\xa4$\xa5:\x84\x1c\r\x0c') or \
           (tile.md5 == b'\xbd\xa2\xf1\xf7\x1cb\xaf\x8b\x9a\x07\xa0\xfb41W\xfd') or \
           (tile.md5 == b'\xd5z\xba0\xb2\xaaJ\xb4\xedg\x12\xc7?\x96\xf1b'):
          tile.dup = True

  def mark_dups(self):
    for i,session1 in enumerate(self.sessions):
      for j,tile1 in enumerate(session1.tiles):
        if not tile1.dup:
          for tile2 in session1.tiles[j+1:]:
            if not tile2.dup:
              if tile1.md5 == tile2.md5:
                tile1.dup = True
                tile2.dup = True
          if tile1.dup:
            for session2 in self.sessions[i+1:]: #FIXME see mapmerger.d
              for tile3 in session2.tiles:
                if not tile3.dup:
                  if tile1.md5 == tile3.md5:
                    tile3.dup = True
          if tile1.dup:
            print("DUP: {} {}".format(tile1.path, tile1.md5))

  def find_pair(self, count):
    for i,session1 in enumerate(self.sessions):
      for j,session2 in enumerate(self.sessions[i+1:]):
        shift = None
        cnt = 0
        diff_shift = False
        for tile1 in session1.tiles:
          if diff_shift:
            break
          for tile2 in session2.tiles:
            if (not tile1.dup) and (not tile2.dup):
              if tile1.md5 == tile2.md5:
                s = (tile1.x - tile2.x, tile1.y - tile2.y)
                if shift == None:
                  shift = s
                  cnt = 1
                elif shift == s:
                  cnt += 1
                else:
                    diff_shift = True
                    print("DIFF SHIFT {}/{} - {}/{} {}".format(session1.name, tile1.name, session2.name, tile2.name, tile1.md5))
                    break
        if not diff_shift:
          if cnt >= count:
            return struct(i=i, j=i+j+1, shift=shift, count=cnt)
    return None

  def merge_pair(self, pair):
    print(self.sessions[pair.i].name, '->', self.sessions[pair.j].name, pair.shift, pair.count)
    dx, dy = pair.shift
    session1 = self.sessions[pair.i]
    session2 = self.sessions[pair.j]
    #session1.tiles = {(x-dx,y-dy):v for (x,y),v in session1.tiles.items()}
    for i,tile1 in enumerate(session1.tiles):
      tile1.x -= dx
      tile1.y -= dy
      i = None
      for j,t in enumerate(session2.tiles):
        if (tile1.x == t.x) and (tile1.y == t.y):
          i = j
          break
      if i == None:
        session2.tiles.append(tile1)
      elif tile1.ctime > session2.tiles[i].ctime:
        session2.tiles[i] = tile1
    del self.sessions[pair.i]

  def merge(self, count):
    while True:
      pair = self.find_pair(count)
      if (pair == None):
        break
      self.merge_pair(pair)

  def save(self, path):
    path = join(self.path, path)
    if exists(path):
      rmtree(path)
    for session in self.sessions:
      session_path = join(path, session.name)
      makedirs(session_path)
      for tile in session.tiles:
        new_tile_path = join(session_path, 'tile_'+str(tile.x)+'_'+str(tile.y)+'.png')
        copyfile(tile.path, new_tile_path)
        copystat(tile.path, new_tile_path)


print("init")
mm = mapmerger("tiles/_1/")
#print("cleans")
#mm.mark_clean_tiles()
print("dups")
mm.mark_dups()
#for i in range(10, 1, -1):
#  print("merge", i)
#  mm.merge(i)
print("merge")
mm.merge(2)
print("save")
mm.save('merged')
print("done")
