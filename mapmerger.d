import std.digest.md;
import std.stdio;
import std.file;
import std.path;
import std.string;
import std.conv;
import std.datetime;
import std.getopt;


struct Tile {
  string path;
  ubyte[16] hash;
  int x;
  int y;
  SysTime time;
  bool dup;
  bool correct;

  this (DirEntry de) {
    this.correct = false;

    if (extension(de.name) != ".png") return;
    auto name = baseName(stripExtension(de.name));
    if (indexOf(name, "tile_") != 0) return;
    if (countchars(name, "_") != 2) return;
    auto tmp = name[5..$];
    auto i = indexOf(tmp, '_');
    auto x = tmp[0..i];
    auto y = tmp[(i+1)..$];
    if ((!x.isNumeric) || (!y.isNumeric)) return;
    this.x = to!int(x);
    this.y = to!int(y);
    this.time = de.timeLastModified;

    //TODO if this.use_sha use sha instead of md5
    this.hash = digest!MD5(read(de.name));
    //auto md5 = new MD5Digest();
    //this.hash = md5.digest(read(de.name));

    this.path = de.name;
    this.dup = false;
    this.correct = true;
  }
};


struct CompareResult {
  int equal_tiles = 0;
  bool diff_shift = false;
  int dx;
  int dy;
};


struct Session {
  string path;
  Tile[] tiles;
  bool active;

  this (string path, bool verbose) {
    this.path = path;
    this.active = true;
    foreach (t; dirEntries(path, SpanMode.shallow)) {
      if (t.isFile) {
        auto tile = Tile(t);
        if (tile.correct) {
          this.tiles ~= tile;
        } else {
          if (verbose) writeln("ignore: ", t);
        }
      }
    }
  }

  auto compare_with (Session s) {
    auto res = CompareResult();
    foreach (t1; this.tiles) {
      if (!t1.dup) {
        foreach (t2; s.tiles) {
          if (!t2.dup) {
            if (t1.hash == t2.hash) {
              auto dx = t1.x - t2.x;
              auto dy = t1.y - t2.y;
              if (res.equal_tiles > 0) {
                if ((dx != res.dx) || (dy != res.dy)) {
                  res.diff_shift = true;
                  break;
                }
              } else {
                res.dx = dx;
                res.dy = dy;
              }
              ++res.equal_tiles;
            }
          }
        }
      }
      if (res.diff_shift) break;
    }
    return res;
  }

  void comparison_info (Session s) {
    foreach (t1; this.tiles) {
      if (!t1.dup) {
        foreach (t2; s.tiles) {
          if (!t2.dup) {
            if (t1.hash == t2.hash) {
              auto dx = t1.x - t2.x;
              auto dy = t1.y - t2.y;
              writeln(format("%11s %-38s %-38s %s",
                        format("(%s,%s)", dx, dy),
                        buildPath(baseName(dirName(t1.path)), baseName(t1.path)),
                        buildPath(baseName(dirName(t2.path)), baseName(t2.path)),
                        toHexString(t1.hash)));
            }
          }
        }
      }
    }
  }

  void shift (int dx, int dy) {
    foreach (ref t; this.tiles) {
      t.x -= dx;
      t.y -= dy;
    }
  }
    
  void merge_to (ref Session s) {
    foreach (ref t1; this.tiles) {
      bool found = false;
      foreach (ref t2; s.tiles) {
        if ((t1.x == t2.x) && (t1.y == t2.y)) {
          if (t1.time > t2.time) {
            t2 = t1;
          }
          found = true;
          break;
        }
      }
      if (!found) {
        s.tiles ~= t1;
      }
    }
  }
};


alias ubyte[] harray;


/*
string hex_string(ubyte[] a) {
  string res;
  foreach (i; a) {
    res ~= format("%02X", i);
  }
  return res;
}
*/


class mapmerger {
  string path;
  bool verbose;
  Session[] sessions;

  this (string path, bool verbose) {
    this.path = path;
    this.verbose = verbose;
    if (this.verbose) { writeln("init..."); }
    foreach (s; dirEntries(this.path, SpanMode.shallow)) {
      if (s.isDir) {
        auto session = Session(s, this.verbose);
        if (session.tiles.length > 0) {
          this.sessions ~= session;
        } else {
          if (this.verbose) writeln("empty session: ", baseName(s));
        }
      }
    }
    if (this.verbose) writeln("sessions total: ", this.sessions.length);
  }

  void mark_dups () {
    if (this.verbose) { writeln("mark duplicates..."); }
    foreach (i, ref s1; this.sessions) {
      if (!s1.active) continue;
      foreach (j, ref t1; s1.tiles) {
        if (t1.dup) continue;
        foreach (ref t2; s1.tiles[(j+1)..$]) {
          if (t2.dup) continue;
          if (t1.hash == t2.hash) {
            t1.dup = true;
            t2.dup = true;
          }
        }
        if (t1.dup) {
          foreach (k, ref s2; this.sessions) {
            if (!s2.active) continue;
            if (i == k) continue;
            foreach (ref t2; s2.tiles) {
              if (t2.dup) continue;
              if (t1.hash == t2.hash) {
                t2.dup = true;
              }
            }
          }
          if (this.verbose) writeln("dup: ", t1.path, " ", toHexString(t1.hash));
          //std.file.copy(t1.path, baseName(t1.path));
        }
      }
    }
  }
  
  void mark_hashes (harray[] hashes) {
    if (this.verbose) { writeln("mark ignorable hashes..."); }
    foreach (ref s; this.sessions) {
      if (!s.active) continue;
      foreach (ref t; s.tiles) {
        if (t.dup) continue;
        foreach (hash; hashes) {
          if (t.hash == hash) {
            t.dup = true;
            break;
          }
        }
      }
    }
  }

  bool check_for_diff_shifts () {
    bool found = false;
    if (this.verbose) { writeln("check for different shifts..."); }
    foreach (i, ref s1; this.sessions) {
      if (!s1.active) continue;
      foreach (ref s2; this.sessions[(i+1)..$]) {
        if (!s2.active) continue;
        auto cmp = s1.compare_with(s2);
        if (cmp.diff_shift) {
          found = true;
          if (this.verbose) {
            writeln("diff: ", baseName(s1.path), " <=> ", baseName(s2.path));
            s1.comparison_info(s2);
          }
        }
      }
    }
    return found;
  }
  
  bool merge_once (uint enough) {
    foreach (i, ref s1; this.sessions) {
      if (!s1.active) continue;
      foreach (ref s2; this.sessions[(i+1)..$]) {
        if (!s2.active) continue;
        auto cmp = s1.compare_with(s2);
        if ((!cmp.diff_shift) && (cmp.equal_tiles >= enough)) {
          s1.shift(cmp.dx, cmp.dy);
          if (this.verbose) {
            writeln(format("merge: %-22s => %-22s equals: %s shift: (%s,%s)",
                    baseName(s1.path),
                    baseName(s2.path),
                    cmp.equal_tiles,
                    cmp.dx, cmp.dy));
          }
          s1.merge_to(s2);
          s1.active = false;
          return true;
        }
      }
    }
    return false;
  }
  
  void merge (uint enough) {
    if (this.verbose) { writeln("merge..."); }
    while (this.merge_once(enough)) {}
  }
  
  void save (string path) {
    if (this.verbose) { writeln("save..."); }
    //path = buildPath(this.path, path);
    if (exists(path) && isDir(path)) {
      rmdirRecurse(path);
    }
    mkdirRecurse(path);
    foreach (s; this.sessions) {
      if (s.active) {
        auto spath = buildPath(path, baseName(s.path));
        mkdir(spath);
        foreach (t; s.tiles) {
          std.file.copy(t.path, buildPath(spath, "tile_" ~ to!string(t.x) ~ "_" ~ to!string(t.y) ~ ".png"));
        }
      }
    }
  }
};


//TODO use linked lists instead of arrays (for sessions and tiles)


auto stringToHash (string s) {
  ubyte[] arr;
  for (uint i=0; i<s.length; i+=2) arr ~= s[i..i+2].to!ubyte(16);
  return arr;
}


void print_help () {
  writeln("usage: mapmerger [options]");
  writeln("  options:");
  writeln("    -h, --help             show this hint and exit");
  writeln("    -v, --verbose          set verbose");
  writeln("    -i, --ignore MD5_HASH  add MD5_HASH to ignore list to avoid comparing tiles with this hash");
  writeln("    -s, --src PATH         path to unmerged maps (default is \".\")");
  writeln("    -d, --dst PATH         path to save merged maps to (dafault is \"./merged/\")");
  writeln("    -e, --enough           number of tiles with equal hash enough for merge (default is 2)");
  writeln("    -g, --debug            run program in debug mode. forces verbose on");
}


void main(string[] args) {
  bool verbose = false;
  bool help = false;
  bool force = false;
  bool dbg = false;
  string[] hash_strings;
  string src_path = getcwd();
  string dst_path = buildPath(src_path, "merged");
  uint enough = 2;
  getopt(args, config.passThrough,
    "verbose|v", &verbose,
    "help|h", &help,
    "ignore|i", &hash_strings,
    "src|s", &src_path,
    "dst|d", &dst_path,
    "force|f", &force,
    "enough|e", &enough,
    "debug|g", &dbg);
  //TODO show usage on unknown option

  if (dbg) verbose = true;

  if (verbose) {
    writeln("MapMerger 3.1");
    writeln("");
  }

  if (help) {
    print_help();
    return;
  }

  src_path = absolutePath(src_path);
  dst_path = absolutePath(dst_path);
  if (src_path == dst_path) {
    writeln("source and destination paths are the same");
    return;
  }
  if (dst_path == getcwd()) {
    writeln("destination path is equal to current working directory");
    return;
  }
  if (!exists(src_path)) {
    writeln("source path is not exists: \"", src_path, "\"");
    return;
  }
  if (!isDir(src_path)) {
    writeln("source path is not a dir: \"", src_path, "\"");
    return;
  }
  if (verbose) {
    writeln("source path: ", src_path);
    writeln("dest-n path: ", dst_path);
  }

  //TODO destination path is not empty use --force to silently overwrite dest dir
  //TODO MD5 collision test
  //TODO diff shifts highlight on tiles
  //TODO adequate progress bars

  hash_strings ~= "ECAA8D5F4648FDCD8B7A56C56DAAA42C";
  hash_strings ~= "D57ABA30B2AA4AB4ED6712C73F96F162";
  hash_strings ~= "D6B28F269CBF68AB54DFD38CF65B94C5";
  hash_strings ~= "F39465CD146F4F65A424A53A841C0D0C";
  hash_strings ~= "BDA2F1F71C62AF8B9A07A0FB343157FD";
  hash_strings ~= "FBA314342173BFD1E48D70E741B1C379";
  hash_strings ~= "AD1E0DA2671C0241BEC6506C2F13A271";
  harray[] hashes;
  foreach (hs; hash_strings) hashes ~= stringToHash(hs);

  if (verbose) {
    foreach (h; hashes) writeln("ignore hash: ", toHexString(h));
  }

  auto mm = new mapmerger(src_path, verbose);
  mm.mark_hashes(hashes);
  mm.mark_dups();
  if (dbg) {
    while (!mm.check_for_diff_shifts()) {
      if (!mm.merge_once(enough)) break;
    }
  } else {
    if (verbose) mm.check_for_diff_shifts();
    mm.merge(enough);
    if (verbose) mm.check_for_diff_shifts();
  }
  mm.save(dst_path);
}

